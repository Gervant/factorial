//Рекурсия используется для упрощения кода в котором можно было бы использовать более очевидные методы по типу циклов,
//которые в свою очередь - примитивны в своем исполнении.

let factorialNumber = null;

do {
    factorialNumber = +prompt('введите число')
} while (!factorialNumber || isNaN(factorialNumber))

function pow(x, n) {
    if (n === 1) {
        return x;
    } else {
        return x * pow(x + 1, n - 1);
    }
}

console.log( pow(1, factorialNumber) );

//Без рекурсии
let number = 1;
let iteration = 1
for (let i = 1;i <= factorialNumber; i++){
    number *= iteration
    iteration += 1
    console.log(number)
}
